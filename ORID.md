## O
- When we use the update and delete statements, we should first write the condition.
- We learnt history of development of spring data jpa.
- Spring Data JPA make it easy to easily implement JPA based repositories.
- Learned how to connect to a database in a Spring boot project, how to add annotations on entity to declare the primary key and primary key policy so that Spring can read it.
## R
- When I just declare a method name in the interface I can achieve a query in the database based on a property which is amazing.
## I
- With JPA, we can manipulate the database without having to manually write SQL.
- Using the h2 database instead of mysql for unit testing is fast, and allows persistence to be turned off and restored to the initial state as soon as each use case is executed.
## D
- I still need to understand JPA configuration well.