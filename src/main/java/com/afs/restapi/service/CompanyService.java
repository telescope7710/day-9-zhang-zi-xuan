package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {


    private final CompanyJPARepository companyJPARepository;
    private final EmployeeJPARepository employeeJPARepository;

    public CompanyService(CompanyJPARepository companyJPARepository, EmployeeJPARepository employeeJPARepository) {

        this.companyJPARepository = companyJPARepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public List<Company> findAll() {
        return companyJPARepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companyJPARepository.findAll(PageRequest.of(page-1,size)).toList();
    }

    public Company findById(Long id) {
        Company company = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = employeeJPARepository.findByCompanyId(company.getId());
        company.setEmployees(employees);
        return company;
    }

    public void update(Long id, Company company) {
        Optional<Company> optionalCompany = companyJPARepository.findById(id);
        optionalCompany.ifPresent(previousCompany -> previousCompany.setName(company.getName()));
        companyJPARepository.save(optionalCompany.orElseThrow(CompanyNotFoundException::new));
    }

    public Company create(Company company) {
        return companyJPARepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeJPARepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
